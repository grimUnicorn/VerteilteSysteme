package org.apache.camel.archetypes.camle;

import java.util.Scanner;

import org.apache.camel.main.Main;

import chat.MainClient;

/**
 * A Camel Application
 */
public class MainApp {

    /**
     * A main() so we can easily run these routing rules in our IDE
     */
    public static void main(String... args) throws Exception {
        Main main = new Main();
        main.enableHangupSupport();
        final MyRouteBuilder builder = new MyRouteBuilder();
        System.out.println("Enter your name:");
        builder.myName = new Scanner(System.in).nextLine();
        main.addRouteBuilder(builder);
        main.run(args);
    }

}

