package org.apache.camel.archetypes.camle;

import java.util.Date;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.camel.builder.RouteBuilder;

/**
 * A Camel Java DSL Router
 */
public class MyRouteBuilder extends RouteBuilder {

    /**
     * Let's configure the Camel routing rules using Java code...
     */
	public String myName = "default";
	
    public void configure() {

        // here is a sample which processes the input files
        // (leaving them in place - see the 'noop' flag)        
        
    	//from("file:src/data?noop=true").to("stomp:queue:TEST");
        
        // from("stomp:queue:TEST").process(new Processor() {
			
			//@Override
		//	public void process(Exchange exchange) throws Exception {
			//	log.info("received new message "+new Date());
			//	log.info(" - " + exchange.getIn().getBody(String.class));
			//	
			//}
		//}).transform(body().convertToString()).to("file:target/messages");
        
        from("stream:in").split().tokenize("\n").dynamicRouter(method(DynamicRouterBean.class, "route"));
        
        from("stomp:queue:"+myName).process(new Processor() {
			
			@Override
			public void process(Exchange exchange) throws Exception {
				log.info("received new message "+new Date());
				log.info(" - " + exchange.getIn().getBody(String.class));
				
			}
		}).transform(body().convertToString()).to("stream:out");
    }

}
