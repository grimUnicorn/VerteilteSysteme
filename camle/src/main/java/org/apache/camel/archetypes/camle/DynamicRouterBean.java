package org.apache.camel.archetypes.camle;

import org.apache.camel.Exchange;
import org.apache.camel.Header;

public class DynamicRouterBean {
	public String route(String body, @Header(Exchange.SLIP_ENDPOINT) String previousRoute) {
		return "stomp:queue:"+body.split("@")[0];
	}
}
