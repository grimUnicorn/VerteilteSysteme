import socket
import sys

sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

server_address = ('localhost', 4321)
print >> sys.stderr, 'starting up on %s port %s' % server_address
sock.bind(server_address)

sock.listen(1)

while True:
	print >> sys.stderr, 'waiting for a connection'
	connection, client_address = sock.accept()

#	try:
        print >> sys.stderr, 'connection from', client_address


	data = connection.recv(1024)
	print >> sys.stderr, 'received "%s"' % data
	data = data [:-1]
	if(data == 'exit'):
		sys.exit(0)

	data = 'You entered ' + data + '\n'

	connection.sendall(data)

	connection.close()
