package versuch;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.SocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.DatagramChannel;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.nio.channels.spi.AbstractSelectableChannel;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class Alternative {
	
	Set<SocketChannel> socketList;
	Set<SocketAddress> udpClients;
	DatagramChannel datagram;
	
	public Alternative() {
		socketList = new HashSet<SocketChannel>();
		udpClients = new HashSet<SocketAddress>();
	}
	
	public void run() throws IOException {
		Selector selector = Selector.open();
		
		ServerSocketChannel channel = ServerSocketChannel.open();
		channel.socket().bind(new InetSocketAddress(4321));
		channel.configureBlocking(false);
		
		SelectionKey acceptKey = channel.register(selector, SelectionKey.OP_ACCEPT);
		
		datagram = DatagramChannel.open();
		datagram.configureBlocking(false);
		datagram.bind(new InetSocketAddress(4322));
		SelectionKey datagramKey = datagram.register(selector, SelectionKey.OP_READ, null);
		
		
		
		while(true) {
			selector.select();
			Set<SelectionKey> keys = selector.selectedKeys();
			
			for(SelectionKey key : keys) {
				if(key.isAcceptable() && key.equals(acceptKey)) {
					SocketChannel socket = channel.accept();
					if(socket==null)
						break;
					socket.configureBlocking(false);
					socket.register(selector, SelectionKey.OP_READ);
					System.out.println("New TCP Sock: "+socket);
					socketList.add(socket);
				}else if(key.isReadable() && !key.equals(datagramKey)) {
					SocketChannel socket = (SocketChannel) key.channel();
					ByteBuffer buffer = ByteBuffer.allocate(1024);
					int read = socket.read(buffer);
					if(read>0) {
						System.out.println("New TCP Msg: "+buffer);
						broadcastFromSocket(buffer, socket);
					}
				}else if(key.equals(datagramKey)) {
					System.out.println("Datagram");
					ByteBuffer buffer = ByteBuffer.allocate(1024);
					SocketAddress adr = datagram.receive(buffer);
					
					if(adr!=null) {
						udpClients.add(adr);
						this.broadcast(buffer, adr);
					}
					
				}
			}
		}
		
	}
	
	private void broadcast(ByteBuffer buffer, SocketAddress sadr) throws IOException {
		for(SocketChannel socket: socketList) {
			buffer.flip();
			while(buffer.hasRemaining()) {
				socket.write(buffer);
			}
		}
		for(SocketAddress adr : udpClients) {
			if(!adr.equals(sadr)) {
				buffer.flip();
				this.datagram.send(buffer, adr);
			}
		}
		
	}

	public void broadcastFromSocket(ByteBuffer buffer, AbstractSelectableChannel src) throws IOException {	
		for(SocketChannel socket: socketList) {
			if(!socket.equals(src)) {
				buffer.flip();
				while(buffer.hasRemaining()) {
					socket.write(buffer);
				}
				
			}
		}
		for(SocketAddress adr : udpClients) {
			buffer.flip();
			int send = this.datagram.send(buffer, adr);
			System.out.println("D: "+send);
		}
	}
}
