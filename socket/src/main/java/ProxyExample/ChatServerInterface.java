package ProxyExample;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.List;

public interface ChatServerInterface extends Remote {
	public void addMessage(String message) throws RemoteException;
	
	public List<String> pollMessage(int fromPosition) throws RemoteException;
}
