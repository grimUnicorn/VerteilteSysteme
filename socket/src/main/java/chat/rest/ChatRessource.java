package chat.rest;

import java.io.IOException;
import java.util.List;

import org.restlet.ext.jackson.JacksonRepresentation;
import org.restlet.representation.Representation;
import org.restlet.resource.Get;
import org.restlet.resource.Put;
import org.restlet.resource.ServerResource;

import chat.entities.Message;
import chat.hibernate.ChatDatabase;
import reststuff.World;

public class ChatRessource extends ServerResource {
	
	ChatDatabase cdb = new ChatDatabase();
	
	@Get
	public Representation getAllMessages() {
		return new JacksonRepresentation<List<Message>>(cdb.getAllMessages());
	}
	
	@Put
	public void update(Representation rep) throws IOException {
		if(getRequestAttributes().containsKey("id")) {
			System.out.println("id is "+getRequestAttributes().get("id"));
		}
		
		JacksonRepresentation<Message> wr = new JacksonRepresentation<Message>(rep,Message.class);
		
		Message w = wr.getObject();
		System.out.println("world has message "+w.getMessage());
		cdb.persist(w);
	}
}

