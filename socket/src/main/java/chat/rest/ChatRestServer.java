package chat.rest;

import org.restlet.Component;
import org.restlet.data.Protocol;

public class ChatRestServer {

	public static void main(String[] args) throws Exception {

		Component component = new Component();
		component.getServers().add(Protocol.HTTP, 8081);
		component.getDefaultHost().attach("/chat", new ChatApplication());
		component.start();
	}
}
