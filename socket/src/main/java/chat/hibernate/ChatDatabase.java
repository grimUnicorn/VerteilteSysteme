package chat.hibernate;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import org.hibernate.Criteria;
import org.hibernate.Session;

import chat.entities.Message;

public class ChatDatabase {

	EntityManager manager;
	EntityManagerFactory factory;
	
	public ChatDatabase() {
		this.factory = Persistence.createEntityManagerFactory("emf");
		this.manager = this.factory.createEntityManager();
	}
	
	public void persist(Message msg) {
		this.manager.getTransaction().begin();
		this.manager.persist(msg);
		this.manager.getTransaction().commit();
	}
	
	public List<Message> getAllMessages(){
		Session session  = (Session) this.manager.getDelegate();
		Criteria cr = session.createCriteria(Message.class);
		return cr.list();
	}
	
}
