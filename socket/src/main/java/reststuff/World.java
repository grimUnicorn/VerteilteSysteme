package reststuff;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

public class World {

	public String message;
	
	public String password = "superSecure";
	
	public World() {
		
	}

	@JsonIgnoreProperties("password")
	public World(String string) {
		this.message = string;
	}

}
