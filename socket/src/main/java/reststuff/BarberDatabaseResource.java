package reststuff;

import java.io.IOException;
import java.util.List;

import org.restlet.ext.jackson.JacksonRepresentation;
import org.restlet.representation.Representation;
import org.restlet.resource.Get;
import org.restlet.resource.Put;
import org.restlet.resource.ServerResource;

import databasestuff.Barber;
import databasestuff.Hibernate;

public class BarberDatabaseResource extends ServerResource {

	Hibernate mydb = new Hibernate();
	
	@Get
	public Representation getAll() {		
		return new JacksonRepresentation<List<Barber>>(mydb.getAllBarbers());
	}
	
	@Put
	public void update(Representation rep) throws IOException {
		if(getRequestAttributes().containsKey("id")) {
			System.out.println("id is "+getRequestAttributes().get("id"));
		}
		
		JacksonRepresentation<World> wr = new JacksonRepresentation<World>(rep,World.class);
		
		World w = wr.getObject();
		System.out.println("world has message "+w.message);
	}
}
