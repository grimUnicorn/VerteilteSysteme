package reststuff;

import org.restlet.Application;
import org.restlet.Restlet;
import org.restlet.routing.Router;

public class HelloWorldApplication extends Application {
	
	@Override
	public Restlet createInboundRoot() {
		Router router = new Router(getContext());
		
		router.attach("/hello/{id}", HelloWorldResource.class);
		router.attach("/db/person", PersonDatabaseResource.class);
		router.attach("/db/barber",BarberDatabaseResource.class);
		
		return router;
	}
}
