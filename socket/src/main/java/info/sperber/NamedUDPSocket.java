package info.sperber;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.util.Arrays;
import java.util.Scanner;

public class NamedUDPSocket extends NameSocket{

	DatagramPacket info;
	DatagramSocket socket;
	
	public NamedUDPSocket(int n, DatagramPacket info, DatagramSocket socket) {
		super(n);
		this.info = info;
		this.socket = socket;
	}

	

	@Override
	public void printOut(String msg) {
		if(msg.charAt(msg.length()-1) != '\n') {msg+="\n";}
		DatagramPacket packet = new DatagramPacket(msg.getBytes(), msg.length());
		packet.setPort(info.getPort());
		packet.setAddress(info.getAddress());
		
		try {
			socket.send(packet);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}



	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((info == null) ? 0 : info.getSocketAddress().hashCode());
		return result;
	}



	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		NamedUDPSocket other = (NamedUDPSocket) obj;
		if (info.getSocketAddress() == null) {
			if (other.info.getSocketAddress() != null)
				return false;
		} else if (!info.getSocketAddress().equals(other.info.getSocketAddress()))
			return false;
		return true;
	}	
}
