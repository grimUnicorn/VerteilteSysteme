package info.sperber;

import java.util.HashSet;
import java.util.Set;

public class Chatroom {
	Set<NameSocket> clientList = new HashSet<NameSocket>();
	
	
	public synchronized boolean add(NameSocket socket) {
		
		socket.setRoom(this);
		return this.clientList.add(socket);
	}
	
	
	public void broadCast(String msg, NameSocket sender) {
		for(NameSocket client: clientList) {
			if(client.equals(sender)) {
				continue;
			}
			
			client.printOut(sender.number+": "+msg);
		}
	}
}
