package info.sperber;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.ServerSocket;
import java.net.Socket;
import java.nio.channels.DatagramChannel;
import java.util.Arrays;

public class UDPHandler extends Thread implements Runnable {

	private Chatroom room;

	public UDPHandler(Chatroom room) {
		this.room = room;
	}

	@Override
	public void run() {
		try {
			DatagramSocket client = new DatagramSocket(4322);
			while (true) {
				byte[] buffer = new byte[1024];
				DatagramPacket packet = new DatagramPacket(buffer, buffer.length);
				client.receive(packet);	
				NameSocket nameS = new NamedUDPSocket(NameCreator.getName(), packet, client);				
				
				if(room.add(nameS)) {
					nameS.printOut("You are: " + nameS.number+"\n");
				}else {
					NameCreator.reduce();
				}
				
				room.broadCast(new String(Arrays.copyOf(packet.getData(), packet.getLength())), nameS);
				
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

}
