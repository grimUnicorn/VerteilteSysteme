package info.sperber;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.Scanner;

public class NamedTCPSocket extends NameSocket {

	Socket socket;
	
	public NamedTCPSocket(int n, final Socket socket) {
		super(n);
		this.socket = socket;
		
		
		Runnable runable = new Runnable() {
			public void run() {
				while(true) {
					try {
						Scanner in = new Scanner(socket.getInputStream());
						String input = in.nextLine();
						
						room.broadCast(input, NamedTCPSocket.this);
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}	
			}
		};
		
		new Thread(runable).start();
	}
	
	public void printOut(String message) {
		try {
			new PrintWriter(socket.getOutputStream(), true).println(message);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((socket == null) ? 0 : socket.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		NamedTCPSocket other = (NamedTCPSocket) obj;
		if (socket == null) {
			if (other.socket != null)
				return false;
		} else if (!socket.equals(other.socket))
			return false;
		return true;
	}
	
	
	

}
