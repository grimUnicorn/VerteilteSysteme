package info.sperber;

public abstract class NameSocket {
	public int number;
	protected Chatroom room;
	
	public NameSocket(int n) {
		this.number = n;
	}
	
	public abstract void printOut(String msg);
	
	public void setRoom(Chatroom r) {
		this.room = r;
	}
	
	
	
}
