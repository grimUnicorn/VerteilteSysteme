package info.sperber;

public class NameCreator {

	static int name = 0;
	
	public static synchronized int getName() {
		name++;
		return name;
	}
	
	public static synchronized void reduce() {
		name--;
	}
}
