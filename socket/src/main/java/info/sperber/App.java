package info.sperber;

import java.io.IOException;

import versuch.Alternative;

/**
 * Hello world!
 *
 */
public class App {

	public static void mainf(String[] args) throws IOException {
		
		//Alternative alt = new Alternative();
		//alt.run();
		
		Chatroom myroom = new Chatroom();
		TCPHandler tcp = new TCPHandler(myroom);
		UDPHandler udp = new UDPHandler(myroom);
		
		tcp.start();
		udp.start();
	}
}
