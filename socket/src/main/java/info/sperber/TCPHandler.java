package info.sperber;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class TCPHandler extends Thread implements Runnable {

	private Chatroom room;
	
	public TCPHandler(Chatroom room) {
		this.room = room;
	}
	
	@Override
	public void run() {
		try {
			ServerSocket server = new ServerSocket(4321);
			while (true) {
				Socket client = server.accept();
				NameSocket nameS = new NamedTCPSocket(NameCreator.getName(), client);
				room.add(nameS);
				nameS.printOut("You are: "+nameS.number);
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

}
