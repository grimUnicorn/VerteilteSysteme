package databasestuff;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;

public class Main {

	public static void mainff(String[] args) throws InstantiationException, IllegalAccessException, ClassNotFoundException, SQLException {
		// TODO Auto-generated method stub
		Class.forName("com.mysql.jdbc.Driver").newInstance();
		
		Connection conn = DriverManager.getConnection("jdbc:mysql://"+
				"localhost/vorlesung?"+
				"user=root&password=root");
		
		java.sql.PreparedStatement  ps = (java.sql.PreparedStatement) conn.prepareStatement("SELECT * from tabelle");
	    ResultSet rs = ps.executeQuery();
	    
	    while(rs.next()) {
	    	System.out.print(rs.getString(1));
	    	System.out.print("; ");
	    	System.out.print(rs.getString(2));
	    	System.out.println();
	    }
	    
	    ps = (java.sql.PreparedStatement) conn.prepareStatement("INSERT INTO tabelle SET name = ?");
	    ps.setString(1, "test123");
	    ps.execute();
	    System.out.println("finish");
	}

}
