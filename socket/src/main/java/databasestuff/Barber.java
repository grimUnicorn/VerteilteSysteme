package databasestuff;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import databasestuff.entities.Person;
import lombok.Data;

@Entity
@Data

public class Barber {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;
	
	private String name;
	
	@OneToMany(mappedBy="favouriteHairdresser")
	private List<Person> customers = new ArrayList<Person>();
	
	public Barber() {
		name = "Bob";
	}
}
