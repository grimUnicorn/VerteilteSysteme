package databasestuff.entities;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import databasestuff.Barber;
import lombok.Data;

@JsonIgnoreProperties("favouriteHairdresser")
@Data
@Entity
public class Person {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;
	
	private String vorname;
	private String nachname;
	
	
	@ManyToOne
	private Barber favouriteHairdresser;
	
	
	public Person(String v, String n) {
		this.vorname = v;
		this.nachname = n;
	}
	
	public Person() {
		vorname = "Sams";
		nachname = "tag";
	}
}
