package databasestuff;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistryBuilder;
import org.hibernate.service.config.internal.ConfigurationServiceInitiator;

import databasestuff.entities.Person;

public class Hibernate {

	EntityManager manager;
	EntityManagerFactory factory;

	public Hibernate() {
		this.factory = Persistence.createEntityManagerFactory("emf");
		this.manager = factory.createEntityManager();
	}

	public List<Person> getAllPerson() {               
        Session session = (Session) this.manager.getDelegate();          
        Criteria cr = session.createCriteria(Person.class);
        return cr.list();
	}
	
	public List<Barber> getAllBarbers(){
		Session session = (Session) this.manager.getDelegate();          
        Criteria cr = session.createCriteria(Barber.class);
        return cr.list();
	}

	public static void main(String[] args) {
		EntityManagerFactory factory = Persistence.createEntityManagerFactory("emf");
		EntityManager manager = factory.createEntityManager();

		Person p = new Person();
		Barber b = new Barber();

		b.getCustomers().add(p);
		p.setFavouriteHairdresser(b);

		manager.getTransaction().begin();
		manager.persist(p);
		manager.persist(b);
		manager.getTransaction().commit();

	}
}
